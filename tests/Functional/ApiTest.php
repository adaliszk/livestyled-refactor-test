<?php
namespace UserApi\Tests\Functional;

use UserApi\Tests\TestCase;

class ApiTest extends TestCase
{
    /**
     *
     * @test
     */
    public function isGetDoingSomething()
    {
        $this->get('/users/1');
        $this->assertNotEmpty($this->response->getContent());
    }

    /**
     *
     * @test
     */
    public function isPostDoingSomething()
    {
        $this->post('/users');
        $this->assertNotEmpty($this->response->getContent());
    }

    /**
     *
     * @test
     */
    public function isPutDoingSomething()
    {
        $this->put('/users');
        $this->assertNotEmpty($this->response->getContent());
    }

    /**
     *
     * @test
     */
    public function isPatchDoingSomething()
    {
        $this->patch('/users');
        $this->assertNotEmpty($this->response->getContent());
    }

    /**
     *
     * @test
     */
    public function isGetListDoingSomething()
    {
        $this->get('/users');
        $this->assertNotEmpty($this->response->getContent());
    }

    public function sampleRequestURLs()
    {
        return [
            '/users/'     =>  ['/users/', 200],
            '/users/1'    =>  ['/users/1', 200],
            '/users/2'    =>  ['/users/2', 200],
            '/users/3'    =>  ['/users/3', 200],
            '/users/a'    =>  ['/users/a', 500],
            '/a/b'  =>  ['/a/b', 404],
        ];
    }

    /**
     *
     * @param string $url
     * @param int $statusCode
     *
     * @dataProvider sampleRequestURLs
     * @test
     */
    public function isResolvingWithJSON(string $url, int $statusCode)
    {
        $this->get($url);

        $responseContent = $this->response->getContent();
        $this->assertNotEmpty($responseContent);

        $decodedContent = json_decode($responseContent);
        $this->assertEquals(JSON_ERROR_NONE, json_last_error());
    }

    /**
     *
     * @param string $url
     * @param int $statusCode
     *
     * @dataProvider sampleRequestURLs
     * @test
     */
    public function isResolvingWithCorrectStatusCode(string $url, int $statusCode)
    {
        $this->get($url);

        $responseContent = $this->response->getContent();
        $this->assertNotEmpty($responseContent);

        $this->assertSame($statusCode, $this->response->getStatusCode());
    }
}