<?php
namespace UserApi\Tests\Unit\Controllers;

use UserApi\Tests\TestCase;
use UserApi\Controllers\Users;
use ReflectionClass;
use Mockery;

class UsersTest extends TestCase
{
    public function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }

    public function getRequiredMethods(): array
    {
        return [
            'getList method' => ['getList',[]],
            'get method' => ['get',[1]],
        ];
    }

    public function getDataSamples():array
    {
        return [
            'User#1' => ['id'=>'1', 'forName'=>'Aman', 'lastName'=>'Ramkumar'],
            'User#2' => ['id'=>'2', 'forName'=>'Leni', 'lastName'=>'Martin'],
            'User#3' => ['id'=>'3', 'forName'=>'Louis', 'lastName'=>'Dalbe'],
        ];
    }

    /**
     * Testing that the controller has the required methods for the test
     *
     * @param string $methodName
     * @param array $params
     *
     * @dataProvider getRequiredMethods
     * @test
     */
    public function hasRequiredMethod(string $methodName, array $params)
    {
        /** @var \UserApi\Models\Users\Registry $registry */
        $registry = Mockery::mock('UserApi\Models\Users\Registry');
        $class = new Users($registry);

        $className = get_class($class);
        $reflection = new ReflectionClass($className);

        $this->assertTrue(
            $reflection->hasMethod($methodName),
            "Missing {$methodName} in {$className}"
        );
    }

    /**
     *
     * @depends hasRequiredMethod
     * @test
     */
    public function hasResultWithAll()
    {
        echo PHP_EOL.'--- hasResultWithAll()'.PHP_EOL;

        $sampleDatas = [];
        foreach($this->getDataSamples() as $name => $data) {
            $sampleDatas[] = $data;
        }

        echo '$sampleDatas: '.print_r($sampleDatas, true).PHP_EOL;

        /** @var \UserApi\Models\Users\Registry $registry */
        $registry = Mockery::mock('UserApi\Interfaces\UserRegistry')
            ->shouldReceive('getAll')->once()
            ->andReturn($sampleDatas)
            ->mock();

        $class = new Users($registry);

        $returned = $class->getList();
        $this->assertSame($sampleDatas, unserialize($returned->getContent()));
    }

    /**
     *
     *
     * @param int $id
     * @param string $forname
     * @param string $lastname
     *
     * @depends      hasRequiredMethod
     * @dataProvider getDataSamples
     * @test
     */
    public function hasResultWithID(int $id, string $forname, string $lastname)
    {
        $sampleData = ['id'=>$id, 'forName'=>$forname, 'lastName'=>$lastname];
        echo PHP_EOL.'--- hasResultWithID('.json_encode($sampleData).')'.PHP_EOL;

        /** @var \UserApi\Models\Users\Registry $registry */
        $registry = Mockery::mock('UserApi\Interfaces\UserRegistry')
            ->shouldReceive('getById')->once()
            ->andReturn([$sampleData])
            ->mock();

        $class = new Users($registry);

        $returned = $class->get($id);
        $this->assertSame($sampleData, unserialize($returned->getContent()));
    }
}
