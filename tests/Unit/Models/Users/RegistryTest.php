<?php
namespace UserApi\Tests\Unit\Models\Users;

use UserApi\Tests\TestCase;
use UserApi\Models\Users\Registry;
use UserApi\Models\CSV\Datasource;

use ReflectionClass;
use Mockery;

class RegistryTest extends TestCase
{
    public function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }

    public function getRequiredMethods(): array
    {
        return [
            'getById method' => ['getById',[1]],
            'getAll method' => ['getAll',[]],
        ];
    }

    /**
     * Testing that the class has the required methods for the test
     *
     * @param string $methodName
     * @param array $params
     *
     * @dataProvider getRequiredMethods
     * @test
     */
    public function hasRequiredMethod(string $methodName, array $params)
    {
        $dataSource = new Datasource('users', $this->app);
        $user = $this->app->make('UserApi\Interfaces\User');
        $class = new Registry($dataSource, $this->app, $user);

        $className = get_class($class);
        $reflection = new ReflectionClass($className);

        $this->assertTrue(
            $reflection->hasMethod($methodName),
            "Missing {$methodName} in {$className}"
        );
    }

    /**
     * Testing that the class methods has any output
     *
     * @param string $methodName
     * @param array $params
     *
     * @depends hasRequiredMethod
     * @dataProvider getRequiredMethods
     * @test
     */
    public function hasReturnValue(string $methodName, array $params)
    {
        $dataSource = new Datasource('users', $this->app);
        $user = $this->app->make('UserApi\Interfaces\User');
        $class = new Registry($dataSource, $this->app, $user);

        $className = get_class($class);

        $this->assertNotNull(
            $class->$methodName(...$params),
            "{$methodName} doesn't have any output in {$className}"
        );
    }

    /**
     * Testing that the class can return the expected type of values
     *
     * @depends hasReturnValue
     * @test
     */
    public function canReturnUserClasses()
    {
        $dataSource = new Datasource('users', $this->app);
        $user = $this->app->make('UserApi\Interfaces\User');
        $class = new Registry($dataSource, $this->app, $user);

        $userClassName = get_class($user);
        $className = get_class($class);

        $users = [];
        foreach($class->getAll() as $user) {
            $users[] = $user;
        }

        echo 'Returned: '.print_r($users, true).PHP_EOL;

        foreach($users as $user)
        {
            $this->assertSame(get_class($user), $userClassName);
            $this->assertInstanceOf($userClassName, $user);
        }
    }

    /**
     * Testing that the class can return the expected type of value
     *
     * @depends hasReturnValue
     * @test
     */
    public function canReturnSpecificUserClass()
    {
        $dataSource = new Datasource('users', $this->app);
        $user = $this->app->make('UserApi\Interfaces\User');
        $class = new Registry($dataSource, $this->app, $user);

        $userClassName = get_class($user);
        $className = get_class($class);

        $users = [];
        foreach($class->getById(1) as $user) {
            $users[] = $user;
        }

        echo 'Returned: '.print_r($users, true).PHP_EOL;

        $this->assertEquals(1, count($users));
        $this->assertInstanceOf($userClassName, $users[0]);
        $this->assertEquals(1, $users[0]->getId());
    }
}
