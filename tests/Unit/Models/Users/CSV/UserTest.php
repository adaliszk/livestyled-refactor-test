<?php
namespace UserApi\Tests\Unit\Models\Users\CSV;

use UserApi\Tests\TestCase;
use UserApi\Models\Users\CSV\User;

use ReflectionClass;
use Mockery;

class UserTest extends TestCase
{
    private $sampleData = [
        0   =>  1,
        1   =>  'Ádám',
        2   =>  'Liszkai'
    ];

    public function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }

    public function getRequiredMethods(): array
    {
        return [
            'getId method' => ['getId',[], $this->sampleData[0]],
            'getForName method' => ['getForName',[], $this->sampleData[1]],
            'getLastName method' => ['getLastName',[], $this->sampleData[2]],
            'getName method' => ['getName',[], $this->sampleData[1].' '.$this->sampleData[2]],
        ];
    }

    /**
     * Testing that the class has the required methods for the test
     *
     * @param string $methodName
     * @param array $params
     * @param mixed $expectedValue
     *
     * @dataProvider getRequiredMethods
     * @test
     */
    public function hasRequiredMethod(string $methodName, array $params, $expectedValue = null)
    {
        $class = new User();

        $className = get_class($class);
        $reflection = new ReflectionClass($className);

        $this->assertTrue(
            $reflection->hasMethod($methodName),
            "Missing {$methodName} in {$className}"
        );
    }

    /**
     * Testing that the class methods has any output
     *
     * @param string $methodName
     * @param array $params
     * @param mixed $expectedValue
     *
     * @depends hasRequiredMethod
     * @dataProvider getRequiredMethods
     * @test
     */
    public function hasReturnValue(string $methodName, array $params, $expectedValue = null)
    {
        $class = new User($this->sampleData);
        $className = get_class($class);

        $this->assertNotNull(
            $class->$methodName(...$params),
            "{$methodName} doesn't have any output in {$className}"
        );
    }

    /**
     * Testing that the class methods has the correct
     *
     * @param string $methodName
     * @param array $params
     * @param mixed $expectedValue
     *
     * @depends hasReturnValue
     * @dataProvider getRequiredMethods
     * @test
     */
    public function hasCorrectReturnValue(string $methodName, array $params, $expectedValue = null)
    {
        $class = new User($this->sampleData);
        $className = get_class($class);

        $this->assertSame($expectedValue, $class->$methodName(...$params));
    }
}
