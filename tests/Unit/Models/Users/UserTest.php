<?php
namespace UserApi\Tests\Unit\Models\Users;

use UserApi\Tests\TestCase;
use UserApi\Models\Users\User;

use ReflectionClass;
use Mockery;

class UserTest extends TestCase
{
    private $sampleData = [
        'id'        =>  1,
        'forName'   =>  'Ádám',
        'lastName'  =>  'Liszkai'
    ];

    public function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }

    public function getRequiredMethods(): array
    {
        return [
            'getId method' => ['getId',[], $this->sampleData['id']],
            'getForName method' => ['getForName',[], $this->sampleData['forName']],
            'getLastName method' => ['getLastName',[], $this->sampleData['lastName']],
            'getName method' => ['getName',[], $this->sampleData['forName'].' '.$this->sampleData['lastName']],
        ];
    }

    /**
     * Testing that the class has the required methods for the test
     *
     * @param string $methodName
     * @param array $params
     * @param mixed $expectedValue
     *
     * @dataProvider getRequiredMethods
     * @test
     */
    public function hasRequiredMethod(string $methodName, array $params, $expectedValue = null)
    {
        $class = new User();

        $className = get_class($class);
        $reflection = new ReflectionClass($className);

        $this->assertTrue(
            $reflection->hasMethod($methodName),
            "Missing {$methodName} in {$className}"
        );
    }

    /**
     * Testing that the class methods has any output
     *
     * @param string $methodName
     * @param array $params
     * @param mixed $expectedValue
     *
     * @depends hasRequiredMethod
     * @dataProvider getRequiredMethods
     * @test
     */
    public function hasReturnValue(string $methodName, array $params, $expectedValue = null)
    {
        $class = new User($this->sampleData);
        $className = get_class($class);

        $this->assertNotNull(
            $class->$methodName(...$params),
            "{$methodName} doesn't have any output in {$className}"
        );
    }

    /**
     * Testing that the class methods has the correct
     *
     * @param string $methodName
     * @param array $params
     * @param mixed $expectedValue
     *
     * @depends hasReturnValue
     * @dataProvider getRequiredMethods
     * @test
     */
    public function hasCorrectReturnValue(string $methodName, array $params, $expectedValue = null)
    {
        $class = new User($this->sampleData);
        $className = get_class($class);

        $this->assertSame($expectedValue, $class->$methodName(...$params));
    }
}
