<?php
namespace UserApi\Tests\Unit\Models\CSV;

use UserApi\Tests\TestCase;
use UserApi\Models\CSV\Datasource;

use ReflectionClass;
use Mockery;

class DatasourceTest extends TestCase
{
    public function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }

    public function getRequiredMethods(): array
    {
        return [
            'getRows method' => ['getRows',[]],
        ];
    }

    /**
     * Testing that the class has the required methods for the test
     *
     * @param string $methodName
     * @param array $params
     *
     * @dataProvider getRequiredMethods
     * @test
     */
    public function hasRequiredMethod(string $methodName, array $params)
    {
        $class = new Datasource('users', $this->app);
        $className = get_class($class);

        $reflection = new ReflectionClass($className);

        $this->assertTrue(
            $reflection->hasMethod($methodName),
            "Missing {$methodName} in {$className}"
        );
    }

    /**
     * Testing that the class methods has any output
     *
     * @param string $methodName
     * @param array $params
     *
     * @depends hasRequiredMethod
     * @dataProvider getRequiredMethods
     * @test
     */
    public function hasReturnValue(string $methodName, array $params)
    {
        $class = new Datasource('users', $this->app);
        $className = get_class($class);

        $this->assertNotNull(
            $class->$methodName(...$params),
            "{$methodName} doesn't have any output in {$className}"
        );
    }

    /**
     *
     * @depends hasReturnValue
     * @test
     */
    public function canReturnRows()
    {
        $class = new Datasource('users', $this->app);

        $rows = [];
        foreach($class->getRows() as $row)
        {
            $rows[] = $row;
            print_r($row);
        }

        $this->assertGreaterThan(1, count($rows));
    }

}
