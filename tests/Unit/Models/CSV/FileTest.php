<?php
namespace UserApi\Tests\Unit\Models\CSV;

use UserApi\Tests\TestCase;
use UserApi\Models\CSV\File;

use ReflectionClass;
use Mockery;

class FileTest extends TestCase
{
    public function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }

    public function getRequiredMethods(): array
    {
        return [
            'getRows method' => ['getRows',[]],
        ];
    }

    /**
     * Testing that the class has the required methods for the test
     *
     * @param string $methodName
     * @param array $params
     *
     * @dataProvider getRequiredMethods
     * @test
     */
    public function hasRequiredMethod(string $methodName, array $params)
    {
        $sourceFile = realpath($this->app->storagePath().DIRECTORY_SEPARATOR.env('APP_DATADIR','data/').'users.csv');
        $class = new File($sourceFile);
        $className = get_class($class);

        $reflection = new ReflectionClass($className);

        $this->assertTrue(
            $reflection->hasMethod($methodName),
            "Missing {$methodName} in {$className}"
        );
    }

    /**
     * Testing that the class methods has any output
     *
     * @param string $methodName
     * @param array $params
     *
     * @depends hasRequiredMethod
     * @dataProvider getRequiredMethods
     * @test
     */
    public function hasReturnValue(string $methodName, array $params)
    {
        $sourceFile = realpath($this->app->storagePath().DIRECTORY_SEPARATOR.env('APP_DATADIR','data/').'users.csv');
        $class = new File($sourceFile);
        $className = get_class($class);

        $this->assertNotNull(
            $class->$methodName(...$params),
            "{$methodName} doesn't have any output in {$className}"
        );
    }

    /**
     * @depends hasReturnValue
     * @test
     */
    public function canReadDatas()
    {
        $sourceFile = realpath($this->app->storagePath().DIRECTORY_SEPARATOR.env('APP_DATADIR','data/').'users.csv');
        $class = new File($sourceFile);

        $returned = '';
        foreach($class->getRows() as $rowData)
        {
            $returned .= $rowData;
            var_dump($rowData);
        }

        $this->assertNotEmpty($returned);
    }
}
