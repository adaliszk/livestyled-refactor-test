<?php
namespace UserApi\Tests\Unit;

use UserApi\Application;
use UserApi\Tests\TestCase;

/**
 * RestfullAPI\Application Test
 *
 * @package Tests\Unit
 * @author: Ádám Liszkai <adaliszk@gmail.com>
 * @since: v2.0.0
 */
class ApplicationTest extends TestCase
{
    private $basePath;

    public function setUp()
    {
        parent::setUp();

        $this->basePath = realpath(__DIR__.'/../../srv/');
    }


    /**
     * Just a reminder to refactor everywhere if the version is changed
     *
     * @test
     */
    public function checkVersion()
    {
        $composerJson = json_decode(file_get_contents(__DIR__ . '/../../composer.json'));
        $this->assertSame($composerJson->version, $this->app::VERSION);
    }

    /**
     *
     * @test
     */
    public function isBasePathCorrect()
    {
        $this->assertEquals($this->basePath, $this->app->basePath());
    }

    /**
     *
     * @test
     */
    public function canBasePathExtended()
    {
        $this->assertEquals($this->basePath.DIRECTORY_SEPARATOR.'api', $this->app->basePath('api'));
    }

    /**
     *
     * @test
     */
    public function isStoragePathCorrect()
    {
        $this->assertEquals($this->basePath.DIRECTORY_SEPARATOR.'storage', $this->app->storagePath());
    }

    /**
     *
     * @test
     */
    public function canStoragePathExtended()
    {
        $this->assertEquals(
            $this->basePath.DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.'data',
            $this->app->storagePath('data')
        );
    }
}
/* End of file ApplicationTest.php */
/* Location: RestfullAPI\Tests\Unit */
