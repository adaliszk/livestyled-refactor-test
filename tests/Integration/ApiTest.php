<?php
namespace UserApi\Tests\Integration\Controllers;

use UserApi\Tests\TestCase;

class ApiTest extends TestCase
{
    public function getDataSamples()
    {
        $this->createApplication();

        $sourceFile = $this->app->storagePath().DIRECTORY_SEPARATOR.env('APP_DATADIR','data/').'users.csv';
        echo '$sourceFile: '.var_export($sourceFile, true).PHP_EOL;
        echo 'realpath($sourceFile): '.var_export(realpath($sourceFile), true).PHP_EOL;

        $csvContent = file_get_contents(realpath($sourceFile));
        $rows = explode(PHP_EOL, $csvContent);

        $dataSamples = [];
        foreach($rows as $rowContent)
        {
            if (!empty($rowContent))
            {
                echo '$rowContent: '.var_export($rowContent, true).PHP_EOL;

                $c = explode(',', $rowContent);
                $dataSamples["User#{$c[0]}"] = [json_encode(['id'=>(int)$c[0], 'forName'=>$c[1], 'lastName'=>$c[2], 'name'=>$c[1].' '.$c[2]])];
            }
        }

        return $dataSamples;
    }

    /**
     *
     * @test
     */
    public function canGetAll()
    {
        $dataSet = $this->getDataSamples();

        $expectedResult = [];
        foreach($dataSet as $name => $data) {
            $expectedResult[] = json_decode($data[0]);
        }

        $this->get('/users/');

        $responseContent = $this->response->getContent();
        echo '$responseContent: '.print_r($responseContent, true).PHP_EOL;

        $this->assertSame(json_encode($expectedResult), $responseContent);
    }

    /**
     *
     * @param string $expectedJsonContent
     *
     * @dataProvider getDataSamples
     * @test
     */
    public function canGetSpecificResult(string $expectedJsonContent)
    {
        $data = json_decode($expectedJsonContent);
        $this->get("/users/{$data->id}");

        $responseContent = $this->response->getContent();
        echo '$responseContent: '.print_r($responseContent, true).PHP_EOL;

        $this->assertSame($expectedJsonContent, $responseContent);
    }
}