<?php
namespace UserApi\Tests;

use Laravel\Lumen\Testing\TestCase as LumenTestCase;
use UserApi\Application;

/**
 * Base TestCase Class
 *
 * @author Ádám Liszkai <adaliszk@gmail.com>
 * @package RestfullAPI\Tests
 */
class TestCase extends LumenTestCase
{
    /** @var Application */
    protected $app;

    public function createApplication()
    {
        ini_set('display_errors', 'On');
        error_reporting(-1);

        $this->app = require __DIR__ . '/../srv/bootstrap/default.php';

        return $this->app;
    }
}