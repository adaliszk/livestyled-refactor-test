# CSV Userdata Retriever

This is a simple API to retrieve users from a CSV source, it's using Laravel's Lumen framework and to server the requests.

## Deploy & Install

Install instruction is on the [Docker Documentation](https://docs.docker.com/engine/installation/) site  
Run the docker on Dockerfile and you will get a working container.

## Tests

Run PHPUnit with the `phpunit.xml` configuration file,  
you have a few suites to directly tests Units and Functionality.