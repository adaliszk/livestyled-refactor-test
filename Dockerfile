FROM ubuntu:16.04

MAINTAINER Ádám Liszkai <adaliszk@gmail.com>

ENV DEBIAN_FRONTEND noninteractive

# Update repositories
RUN apt-get update

# Add Repository handler libs
RUN apt-get install -y software-properties-common python-software-properties

# Add Ondrej PPA's
RUN LANG=C.UTF-8 add-apt-repository ppa:ondrej/php -y
RUN LANG=C.UTF-8 add-apt-repository ppa:ondrej/apache2 -y

# Update repositories
RUN apt-get update

# Install PHP
RUN apt-get install -y \
	php7.1 \
	php7.1-cli \
	php7.1-common \
	php7.1-mbstring \
	php7.1-json \
	php7.1-memcached

# Install Apache2
RUN apt-get install -y apache2 libapache2-mod-php7.1

# Initialize Apache2
RUN mkdir -p /var/run/apache2
RUN chown root:root /var/run/apache2
RUN a2enmod rewrite

# Install Other Dependencies
RUN apt-get install -y curl git samba cifs-utils nfs-common passwd

# Install composer
RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/bin/composer

# Install SSH & Supervisor
RUN apt-get install -y openssh-server openssh-client supervisor
RUN mkdir -p /var/run/sshd && mkdir -p /var/log/supervisor

# Configure SSH
#RUN ssh-keygen -q -N "" -t dsa -f /etc/ssh/ssh_host_dsa_key && ssh-keygen -q -N "" -t rsa -f /etc/ssh/ssh_host_rsa_key
RUN sed -ri 's/PermitRootLogin without-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
RUN echo 'root:secret' | chpasswd

# Put your own public key at id_rsa.pub for key-based login.
RUN mkdir -p /root/.ssh && touch /root/.ssh/authorized_keys && chmod 700 /root/.ssh
#ADD /home/adaliszk/.ssh/id_rsa.pub /root/.ssh/authorized_keys

# Add Apache2 configuration files
ADD ./docker/vhost.conf /etc/apache2/sites-enabled/000-default.conf

# Add PHP configuration files
ADD ./docker/php.ini /etc/php/7.1/apache2/conf.d/00-php.ini
ADD ./docker/php.ini /etc/php/7.1/cli/conf.d/00-php.ini

# Add Supervisor configuration files
ADD ./docker/supervisor.conf /etc/supervisord.conf

# Add Composer files to the container
ADD ./composer.json /composer.json
ADD ./vendor /vendor

# Add the project source
ADD ./srv /srv

# Add datas to the storage
RUN mkdir -p /srv/storage/data
ADD ./data /srv/storage/data

# Add PHP build in server script
ADD ./artisan /artisan
ADD ./server /server

# Setting Environment to the Project
ENV SERVER_NAME user-api.livestyled.local
ENV DOCUMENT_ROOT /srv/public

ENV APP_ENV testing
ENV APP_DEBUG true
ENV APP_KEY s1V5Am1F34T84DTnPF7Ia5AkOmot0p
ENV APP_TIMEZONE UTC
ENV APP_DATADIR data/

ENV DB_CONNECTION mysql
ENV DB_HOST 127.0.0.1
ENV DB_PORT 3306
ENV DB_DATABASE homestead
ENV DB_USERNAME homestead
ENV DB_PASSWORD secret

ENV CACHE_DRIVER file
ENV QUEUE_DRIVER sync

# Ensure that the production container will run with the www-data user
RUN chown www-data /artisan && chmod +x /artisan
RUN chown www-data /server && chmod +x /server
RUN chown www-data /srv && chmod 777 -R /srv

# Expose ports (SSH,APACHE,APACHE,SUPERVISOR,PHP)
EXPOSE 22 80 443 8000 8080

# Run Supervisor
CMD ["supervisord", "-n"]