<?php

$app->options('/users', function() use ($app)
{
    return response($app->version(), 200, [
        'Access-Control-Allow-Methods' => 'GET',
        'Access-Control-Allow-Credentials' => 'true',
        'Access-Control-Allow-Headers' => 'X-HTTP-Method-Override, Content-Type, Accept'
    ]);
});

$app->get('/users/{id}', 'Users@get');
$app->get('/users', 'Users@getList');
$app->post('/users', 'Users@post');
$app->put('/users/{id}', 'Users@put');
$app->patch('/users/{id}', 'Users@path');
$app->delete('/users/{id}', 'Users@delete');

$app->get('/', function() use ($app) {
    return $app->version();
});