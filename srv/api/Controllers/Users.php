<?php
namespace UserApi\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use UserApi\Interfaces\API\REST;
use UserApi\Interfaces\UserRegistry;

/**
 * Users handler
 *
 * @authod Ádám Liszkai <adaliszk@gmail.com>
 * @package RestfullAPI\Controllers
 */
class Users extends Controller implements REST
{
    /** @var \UserApi\Models\Users\Registry */
    private $users;

    /**
     * Dependency Injection by the Laravel IoC
     *
     * @param UserRegistry $userRegistry (interface concrete defined in the bootstrap file)
     */
    public function __construct(UserRegistry $userRegistry)
    {
        $this->users = $userRegistry;
    }

    /**
     * Getting a user by Id
     *
     * HTTP 200 if found something
     * HTTP 404 if not
     *
     * @param int $id
     *
     * @return Response
     */
    public function get(int $id): Response
    {
        $user = $this->users->getById($id);
        if (count($user) > 0) return response(serialize($user[0]), 200);
        return response(serialize([]), 404);
    }

    /**
     * Getting a user by Id
     *
     * HTTP 200 if found something
     * HTTP 404 if not
     *
     * @return Response
     */
    public function getList(): Response
    {
        $users = [];
        foreach ($this->users->getAll(true) as $user) {
            $users[] = $user;
        }

        if (count($user) > 0) return response(serialize($users), 200);
        return response(serialize([]), 404);
    }

    public function post(): Response
    {
        return response('[]', 405);
    }

    public function put(int $id): Response
    {
        return response('[]', 405);
    }

    public function patch(int $id): Response
    {
        return response('[]', 405);
    }

    public function delete(int $id): Response
    {
        return response('[]', 405);
    }
}
