<?php
namespace UserApi\Controllers;

use Laravel\Lumen\Routing\Controller as LumenController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Base Controller
 *
 * it doesn't have any propose yet.
 *
 * @package RestfullAPI\Controllers
 */
class Controller extends LumenController
{
}
