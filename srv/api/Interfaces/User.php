<?php
namespace UserApi\Interfaces;

use JsonSerializable;

interface User extends JsonSerializable
{
    public function getId(): int;
    public function getForName(): string;
    public function getLastName(): string;
    public function getName(): string;
}