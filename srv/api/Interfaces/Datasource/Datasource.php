<?php
namespace UserApi\Interfaces\Datasource;

use UserApi\Application;

interface Datasource
{
    public function __construct(string $groupName, Application $app);
    public function getRows();
}
