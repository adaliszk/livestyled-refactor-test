<?php
namespace UserApi\Middlewares;

use Closure;
use Illuminate\Http\Response;

class Output
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** @var Response $response */
        $response = $next($request);

        $response->withHeaders([
            'Access-Control-Allow-Origin' => '*',
            'Content-Type' => 'application/json',
        ]);

        $responseContent = $response->getContent();
        $data = @unserialize($responseContent);

        // If it's a serialized PHP Object then convert it to JSON
        if ($data !== false) $response->setContent(json_encode($data));
        // else show it as a message or an error
        elseif ($response->getStatusCode() == 200) $response->setContent(json_encode(['message'=>$responseContent]));
        else $response->setContent(json_encode(['error'=>$responseContent]));

        return $response;
    }
}