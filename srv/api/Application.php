<?php
namespace UserApi;

use Laravel\Lumen\Application as LumenApplication;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;
use Monolog\Logger;

class Application extends LumenApplication
{
    const VERSION = '2.0.0';
    private $BASE_PATH;

    public function __construct()
    {
        $this->BASE_PATH = realpath(__DIR__.'/../');
        parent::__construct($this->BASE_PATH);

        $this->withFacades();
        // $this->withEloquent();
    }

    public function version()
    {
        return 'Users RestAPI (' . self::VERSION . ') powered by ' . parent::version();
    }

    /**
     * Register container bindings for the application.
     *
     * @return void
     */
    protected function registerLogBindings()
    {
        $this->singleton('Psr\Log\LoggerInterface', function () {
            return new Logger('UsersApi', $this->getMonologHandler());
        });
    }
    /**
     * Extends the default logging implementation with additional handlers if configured in .env
     *
     * @return array of type \Monolog\Handler\AbstractHandler
     */
    protected function getMonologHandler()
    {
        $handlers = [];
        $fileName = date('Y-m-d');

        $streamHandler = new StreamHandler(
            storage_path('logs/'.$fileName.'.log'),
            Logger::DEBUG,
            true,
            0666
        );

        $handlers[] = $streamHandler->setFormatter(
            new LineFormatter(
                null,
                'H:i:s',
                true,
                true
            )
        );

        return $handlers;
    }
}