<?php
/**
 * Created by PhpStorm.
 * User: adaliszk
 * Date: 2017.06.12.
 * Time: 9:50
 */

namespace UserApi\Models\CSV;

use UserApi\Application;
use UserApi\Interfaces\Datasource\Datasource as DatasourceInterface;
use Exception;

class Datasource implements DatasourceInterface
{
    private $file;
    private $app;

    public function __construct(string $dataGroup = null, Application $app)
    {
        $this->app = $app;

        if (!empty($dataGroup))
        {
            $fileName = $this->app->storagePath(env('APP_DATADIR','data/')."{$dataGroup}.csv");
            if (file_exists($fileName))
            {
                $this->file = new File($fileName);
            }
            else throw new Exception("Missing datafile: {$fileName}");
        }
    }

    public function getRows()
    {
        foreach ($this->file->getRows() as $rowContent)
        {
            yield new Row($rowContent);
        }
    }
}